package com.parsec.DBConnection;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;
import java.io.File;
import java.io.FileInputStream;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

public class DBConnection {
    
    public Connection ConnectDB()
    {
        Connection con=null;

        try
        {
            File file = new File("src/new.properties");
            FileInputStream fileInput = new FileInputStream(file);
            Properties  props = new Properties();
             props.load(fileInput);
            //String mail= props.getProperty("from");


            System.out.println("Drivers: "+props.getProperty("db_drivers"));
            System.out.println("URL: "+props.getProperty("db_connection_url"));
            System.out.println("Username: "+props.getProperty("db_username"));
            System.out.println("Password: "+props.getProperty("db_password"));

            Class.forName(props.getProperty("db_drivers"));
            con=(Connection) DriverManager.getConnection(props.getProperty("db_connection_url"),props.getProperty("db_username"),props.getProperty("db_password"));

            return con;
        }
        catch(Exception e)
        {
                System.out.println(e);
        }
        return null;

    }

    public void disconnect(PreparedStatement psmt,Statement st, ResultSet rs, Connection con) throws SQLException
    {
            if(psmt != null)
                    psmt.close();
            if(st != null)
                    st.close();
            if(rs != null)
                    rs.close();
            if(con != null)
                    con.close();

            //System.out.println("connection closed successfully");
             System.out.println("\n");
    }

}
