/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.parsec.mars.silentwhisper.main;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;

/**
 * FXML Controller class
 *
 * @author DELL
 */
public class HomeController implements Initializable {


    @FXML
    private Menu menuUser;

    @FXML
    private MenuItem menuItemUser;

    @FXML
    private Menu menuGroups;

    @FXML
    private MenuItem menuItemShowUsers;

    @FXML
    private Menu menuSystemParameters;

    @FXML
    private MenuItem menuItemShowSystemParameters;

    @FXML
    private Menu menuChangepassword;

    @FXML
    private MenuItem menuItemShowChangePassword;

    @FXML
    private Menu menuLogut;

    @FXML
    private MenuItem menuItemShowLogout;
    
    Main m = new Main();
    
    
    
    @FXML
    private void goUsers() throws IOException
    {
            m.showUsers();
    }

    @FXML
    private void goGroups() throws IOException
    {
            m.showGroups();
    }

    @FXML
    private void goSystemParameters() throws IOException
    {
            m.showSystemParameters();
    }

    @FXML
    private void goChangePassword() throws IOException
    {
            m.showChangePassword();
    }

//    @FXML
//    private void goLogout() throws IOException
//    {
//            m.showLogout();
//    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
