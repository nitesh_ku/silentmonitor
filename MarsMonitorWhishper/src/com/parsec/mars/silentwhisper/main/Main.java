/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.parsec.mars.silentwhisper.main;

import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

/**
 *
 * @author DELL
 */
public class Main extends Application {
    
    public static Stage primaryStage;
    public static Stage loginStage;
    private static BorderPane loginBorderLayout;
    private static BorderPane mainLayout;
    
    public static void main(String[] args) {
        launch(args);
    }
    
    @Override
    public void start(Stage stage) throws Exception {
        
        this.primaryStage= stage;
        this.loginStage= stage;
        //this.primaryStage.setTitle("MARS Silent Monitor Whisper Login");
        loginPage();
        
        
        
        /*
        Parent root = FXMLLoader.load(getClass().getResource("/com/parsec/mars/silentwhisper/Home.fxml"));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();*/
    }

    public static void loginPage() throws IOException
    {
        primaryStage.setTitle("MARS Monitor Whisper Login");
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Main.class.getResource("/com/parsec/mars/silentwhisper/main/Login.fxml"));
        loginBorderLayout = loader.load();
        Scene scene = new Scene(loginBorderLayout);

        //mainLayout.setStyle("-fx-background-color: cornsilk;");

        primaryStage.setMaximized(false);
        loginStage.setScene(scene);
        primaryStage.setResizable(false);
        loginStage.show();
    }
    
    
    
    public static void homePage() throws IOException
    {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Main.class.getResource("/com/parsec/mars/silentwhisper/main/Home.fxml"));
        mainLayout = loader.load();
        Scene scene = new Scene(mainLayout);
        
         primaryStage.setTitle("MARS Monitor Whisper Home");
        primaryStage.setMaximized(false);
        primaryStage.setResizable(true);
       //// primaryStage.setMaxWidth(1024);
       // primaryStage.setMaxHeight(720);
        primaryStage.setScene(scene);
        primaryStage.show();
        
    }
    
    public void showUsers() throws IOException
    {
        FXMLLoader loader=new FXMLLoader();
        loader.setLocation(Main.class.getResource("/com/parsec/mars/silentwhisper/user/Users.fxml"));
        BorderPane bpZoneReport=loader.load();
        //primaryStage.setMaximized(true);
        mainLayout.setCenter(bpZoneReport);
    }
    
    public void showGroups() throws IOException
    {
        FXMLLoader loader=new FXMLLoader();
        loader.setLocation(Main.class.getResource("/com/parsec/mars/silentwhisper/groups/Groups.fxml"));
        BorderPane bpZoneReport=loader.load();
        //primaryStage.setMaximized(true);
        mainLayout.setCenter(bpZoneReport);
    }
    
    public void showSystemParameters() throws IOException
    {
        FXMLLoader loader=new FXMLLoader();
        loader.setLocation(Main.class.getResource("/com/parsec/mars/silentwhisper/system/SystemParameters.fxml"));
        BorderPane bpZoneReport=loader.load();
        //primaryStage.setMaximized(true);
        mainLayout.setCenter(bpZoneReport);
    }
    
    public void showChangePassword() throws IOException
    {
        FXMLLoader loader=new FXMLLoader();
        loader.setLocation(Main.class.getResource("/com/parsec/mars/silentwhisper/main/ChangePassword.fxml"));
        BorderPane bpZoneReport=loader.load();
        //primaryStage.setMaximized(true);
        mainLayout.setCenter(bpZoneReport);
    }
    
//    public void showLogout() throws IOException
//    {
//        FXMLLoader loader=new FXMLLoader();
//        loader.setLocation(Main.class.getResource("/com/parsec/mars/silentwhisper/main/InwardPage.fxml"));
//        BorderPane bpZoneReport=loader.load();
//        //primaryStage.setMaximized(true);
//        mainLayout.setCenter(bpZoneReport);
//    }
    
    
    
}
